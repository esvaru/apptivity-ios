//
//  ActivityCollectionViewCell.swift
//  Apptivity
//
//  Created by Django Witmer on 11/7/15.
//  Copyright © 2015 Esvaru. All rights reserved.
//

import UIKit

class ActivityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var activityId:Int = 0;
    
    func setData(data:JSON){
        /*let shadowPath = UIBezierPath(rect: self.bounds);
        self.layer.masksToBounds = false;
        self.layer.shadowColor = UIColor.blackColor().CGColor;
        self.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowPath = shadowPath.CGPath;*/
        
        self.layer.borderWidth = 1;
        self.layer.borderColor = UIColor(rgb: 0xdcdcdb).CGColor;
        
        if let id = data["id"].int {
            activityId = id;
        }
        
        if let title = data["name"].string {
            titleLabel.text = title;
        }
        
        if let city = data["location"].string {
            cityLabel.text = city;
        }
        
        NSLog(data.description);
        
        if let description = data["desciption"].string {
            descriptionLabel.text = description;
        }
        
        if let thumbnailUrl = data["image"].string {
            let url = NSURL(string: thumbnailUrl);
            image.sd_setImageWithURL(url);
            image.hidden = false;
        } else {
            image.image = nil;
            image.hidden = true;
        }
    }
}
