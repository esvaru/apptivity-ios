//
//  ActivityController.swift
//  Apptivity
//
//  Created by Django Witmer on 11/7/15.
//  Copyright © 2015 Esvaru. All rights reserved.
//

import UIKit

class ActivityViewController: UITableViewController {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var userdata = Userdata.sharedInstance;
    var activityId:Int = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        bgView.layer.borderWidth = 1;
        bgView.layer.borderColor = UIColor(rgb: 0xdcdcdb).CGColor;
        
        tableView.rowHeight = UITableViewAutomaticDimension;
        tableView.backgroundColor = UIColor(rgb: 0xfaf8f8);
        
        loadActivity();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadActivity() {
        let manager = AFHTTPRequestOperationManager(baseURL: NSURL(string: baseUrl));
        manager.responseSerializer.acceptableContentTypes!.insert("application/json");
        
        manager.GET(
            String(format: "activity/%@?token=%@", arguments: [String(activityId), userdata.token!]),
            parameters: nil,
            success: { (operation, response) -> Void in
                let jsonResult = JSON(data:operation.responseData!);
                NSLog(jsonResult.description);
                
                if let title = jsonResult["activity"]["name"].string {
                    self.titleLabel.text = title;
                }
                
                if let description = jsonResult["activity"]["desciption"].string {
                    self.descriptionLabel.text = description;
                }
                
                if let location = jsonResult["activity"]["location"].string {
                    self.cityLabel.text = location;
                }
                
                if let username = jsonResult["activity"]["user"]["name"].string {
                    self.userLabel.text = username;
                }
                
                if let thumbnailUrl = jsonResult["activity"]["image"].string {
                    let url = NSURL(string: thumbnailUrl);
                    self.image.sd_setImageWithURL(url);
                    self.image.hidden = false;
                } else {
                    self.image.image = nil;
                    self.image.hidden = true;
                }
            },
            failure: { (operation, error) -> Void in
                if(operation!.responseString != nil) {
                    NSLog("error: %s", operation!.responseString!);
                }
            }
        );
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
