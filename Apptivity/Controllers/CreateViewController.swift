//
//  CreateViewController.swift
//  Apptivity
//
//  Created by Django Witmer on 11/7/15.
//  Copyright © 2015 Esvaru. All rights reserved.
//

import UIKit

class CreateViewController: UITableViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate {
    
    var userdata = Userdata.sharedInstance;
    var selectedImage:UIImage?;

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var titleField: UITextField!
    
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var background: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        background.layer.borderWidth = 1;
        background.layer.borderColor = UIColor(rgb: 0xdcdcdb).CGColor;

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onCreateButtonPressed(sender: AnyObject) {
        let manager = AFHTTPRequestOperationManager(baseURL: NSURL(string: baseUrl));
        manager.responseSerializer.acceptableContentTypes!.insert("application/json");
        
        manager.POST(
            "create?token=" + userdata.token!,
            parameters: [
                "name": titleField.text!,
                "location": "Amsterdam",
                "description": descriptionField.text!
            ],
            constructingBodyWithBlock: {
                formData -> Void in
                
                if(self.selectedImage != nil) {
                    let imageData = UIImageJPEGRepresentation(self.selectedImage!, 0.5);
                    formData.appendPartWithFileData(imageData!, name:"image", fileName:"image.jpg", mimeType:"image/jpeg");
                }
            },
            success: { (operation, response) -> Void in
                //let jsonResult = JSON(data:operation.responseData!);
                self.navigationController?.popViewControllerAnimated(true);
            },
            failure: { (operation, error) -> Void in
                //NSLog(operation!.responseString!);
                self.onFailure(error);
            }
        );
    }
    
    func onFailure(error:NSError?) {
        if(error != nil) {
            NSLog("error: %s", error!.debugDescription);
        } else {
            NSLog("error");
        }
    }
    
    // MARK: - btn actions
    @IBAction func imageBtnPressed(sender: AnyObject) {
        galleryButtonClicked();
        /*
        let actionSheet = UIActionSheet(title: "Add picture", delegate: self, cancelButtonTitle: "cancel", destructiveButtonTitle: nil, otherButtonTitles:  "gallery", "camera");
        
        actionSheet.showInView(self.view);
        */
    }
    
    // MARK: - image picker stuff
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        selectedImage = image;
        imageView.image = selectedImage;
        
        //imgBtn.backgroundColor = UIColor.clearColor();
        //imgBtn.setImage(nil, forState: UIControlState.Normal);
        //showImageView();
        
        imageView.image = image;
        
        picker.dismissViewControllerAnimated(true, completion: nil);
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 1) {
            galleryButtonClicked();
        } else if(buttonIndex == 2) {
            photoButtonClicked();
        }
    }
    
    func galleryButtonClicked() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            //self.imageViewHeightConstraint.constant = 200;
            
            let imag = UIImagePickerController();
            imag.delegate = self;
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            //imag.mediaTypes = [kUTTypeImage];
            //imag.mediaTypes = NSArray(object: kUTTypeImage) as [AnyObject];//[kUTTypeImage];
            imag.allowsEditing = true;
            
            self.presentViewController(imag, animated: true, completion: nil);
        }
    }
    
    
    func photoButtonClicked() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            //self.imageViewHeightConstraint.constant = 200;
            
            let imag = UIImagePickerController();
            imag.delegate = self;
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            //imag.mediaTypes = NSArray(object: kUTTypeImage) as [AnyObject];//[kUTTypeImage];
            imag.allowsEditing = true;
            
            self.presentViewController(imag, animated: true, completion: nil);
        }
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        //self.imageViewHeightConstraint.constant = 0;
        
        picker.dismissViewControllerAnimated(true, completion: nil);
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
