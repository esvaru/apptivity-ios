//
//  ListController.swift
//  Apptivity
//
//  Created by Django Witmer on 11/7/15.
//  Copyright © 2015 Esvaru. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var userdata = Userdata.sharedInstance;
    
    var activities:Array<JSON> = [];
    var refreshControl: UIRefreshControl = UIRefreshControl();
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        refreshControl.addTarget(self, action: "loadActivities", forControlEvents: UIControlEvents.ValueChanged);
        collectionView.addSubview(refreshControl);
        collectionView.backgroundColor = UIColor(rgb: 0xfaf8f8);
        
        let layout = self.collectionView!.collectionViewLayout as! RDHCollectionViewGridLayout;
        layout.lineItemCount = 2;
        layout.itemSpacing = 16;
        layout.lineSpacing = 16;
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        loadActivities();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadActivities() {
        let manager = AFHTTPRequestOperationManager(baseURL: NSURL(string: baseUrl));
        manager.responseSerializer.acceptableContentTypes!.insert("application/json");
        
        manager.GET(
            "list?token=" + userdata.token!,
            parameters: nil,
            success: { (operation, response) -> Void in
                let jsonResult = JSON(data:operation.responseData!);
                NSLog(jsonResult.description);
                if let stream = jsonResult["activities"].array {
                    self.activities = stream;
                    
                    self.refreshControl.endRefreshing();
                    self.collectionView.reloadData();
                }
            },
            failure: { (operation, error) -> Void in
                if(operation!.responseString != nil) {
                    NSLog("error: %s", operation!.responseString!);
                }
                self.refreshControl.endRefreshing();
                self.onFailure(error);
            }
        );
    }
    
    func onFailure(error:NSError?) {
        if(error != nil) {
            NSLog("error: %s", error!.description);
        } else {
            NSLog("error");
        }
    }
    
    // MARK: - Collectionview
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activities.count;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("activity-cell", forIndexPath: indexPath) as! ActivityCollectionViewCell;
        
        let activity = activities[indexPath.item];
        
        cell.setData(activity);
            
        return cell;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "to_activity") {
            let cell = sender as! ActivityCollectionViewCell;
            let controller = segue.destinationViewController as! ActivityViewController;
            controller.activityId = cell.activityId;
        }
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
