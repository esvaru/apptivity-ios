//
//  ViewController.swift
//  Apptivity
//
//  Created by Django Witmer on 11/7/15.
//  Copyright © 2015 Esvaru. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        if(Userdata.sharedInstance.token == nil) {
            registerUser();
        } else {
            performSegueWithIdentifier("to_app", sender: self);
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func registerUser() {
        let manager = AFHTTPRequestOperationManager(baseURL: NSURL(string: baseUrl));
        manager.responseSerializer.acceptableContentTypes!.insert("application/json");
        
        manager.GET(
            "register",
            parameters: nil,
            success: { (operation, response) -> Void in
                let jsonResult = JSON(data:operation.responseData!);
                if let token = jsonResult["token"].string {
                    Userdata.sharedInstance.token = token;
                    Userdata.sharedInstance.saveData();
                    self.performSegueWithIdentifier("to_app", sender: self);
                } else {
                    self.onLoadFail(nil);
                }
            },
            failure: { (operation, error) -> Void in
                self.onLoadFail(error);
            }
        );
    }
    
    func onLoadFail(error:NSError?) {
        if(error != nil) {
            NSLog("error: %s", error!.debugDescription);
        } else {
            NSLog("error");
        }
    }

}

