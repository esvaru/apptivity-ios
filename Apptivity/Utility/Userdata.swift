//
//  Userdata.swift
//  Apptivity
//
//  Created by Django Witmer on 11/7/15.
//  Copyright © 2015 Esvaru. All rights reserved.
//

import Foundation

private let _UserDataSharedInstance = Userdata();

class Userdata:NSObject {
    var token:String?;
    
    override init() {
        super.init();
        loadData();
    }
    
    class var sharedInstance: Userdata {
        return _UserDataSharedInstance
    }
    
    func loadData() {
        let userDefaults = NSUserDefaults.standardUserDefaults();
        
        token = userDefaults.stringForKey("token");
    }
    
    func saveData() {
        let userDefaults = NSUserDefaults.standardUserDefaults();
        
        userDefaults.setValue(token, forKey: "token");
        
        userDefaults.synchronize();
    }
    
    func clear() {
        let userDefaults = NSUserDefaults.standardUserDefaults();
        let appDomain = NSBundle.mainBundle().bundleIdentifier;
        userDefaults.removePersistentDomainForName(appDomain!);
        
        loadData();
    }
}